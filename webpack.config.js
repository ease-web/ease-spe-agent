const path = require('path');
var WebpackCleanPlugin = require('webpack-clean');

module.exports = {
    entry: './ease-spe-agent.js',
    target: 'node',
    node: {
        fs: "empty"
    },
    output: {
        path: path.resolve(__dirname, ""),
        filename: "ease-spe-agent-bundle.js",
    },
    plugins: [
        new WebpackCleanPlugin([
            './ease-spe-agent.js'
        ])
    ]
};
