const bodyParser = require('body-parser');

const express = require('express');
var app = express();

const net = require('net');
var socketClient;

const error = 'ERROR';
const debug = 'DEBUG';
var errorMessage = '';
var dateFormat = require('dateformat');

const version = require('./version.json');

// Constants
const eol = require('os').EOL;
const weol = '\r\n';
const std_send_size = 8192;

const axios = require('axios');

// Parameter with default values
var webServicePort = process.env.WEB_SERVICE_PORT || '2688';
var speIp          = process.env.SPE_IP || '192.168.222.67';
var spePort        = process.env.SPE_PORT || '1344';

logger('Web Service Port : ' + webServicePort);
logger('SPE IP           : ' + speIp);
logger('SPE Port         : ' + spePort);
logger('Version:' + version.version);

// ***********************************************************************
// Configure app to use bodyParser(), this will let us get the data from a POST
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
app.use(bodyParser.json({ limit: '10mb', extended: true }));
  
// ***********************************************************************
// Listen to the App Engine-specified port
app.listen(webServicePort, () => {
  logger(`Server listening on port ${webServicePort}...`);
});

// ***********************************************************************
// Web Service action by POST (POST)
app.post('/antivirus-scan', (req, res) => {
  // ***********************************************************************
  // CheckEmpty
  console.log('Start antivirus-scan');
  // console.log(req.body);    // Debug - See the JSON object, file in Base64 format
  if (req.body['attachment'] == null || req.body['attachment'] === '') {
    // Check ApplicationId, Filename & Attachment(File in base64) (CheckEmpty)
    errorMessage = 'Error: Calling ease-spe-agent by empty values.';
    logger(errorMessage, error);
    res.send(errorMessage);    // Output error to end the web service
    updateLog(req, errorMessage);
  } else {
    // ***********************************************************************
    // Check attachment pattern
    var attachmentArray = req.body['attachment'].split('base64,'); // Prefix: [data:image/jpeg;base64,|data:image/png;base64,|data:application/pdf;base64,]
    var fileContent = "";
    if (attachmentArray.length !== 2) {
      // Attachment not in correct pattern
      errorMessage = 'Incorrect attachment pattern. Attachment:'+req.body['attachment'];
      logger(errorMessage, error);
      res.send(errorMessage);    // Output error to end the web service
      updateLog(req, errorMessage);
    } else {
      //logger('Whole Attachment:'+req.body['attachment'], debug);
      //logger('After base64:'+attachmentArray[1], debug);
      fileContent = attachmentArray[1];
    
      // ***********************************************************************
      // Check options - Send ICAP message to SPE
      var fileBuffer = Buffer.from(fileContent, 'base64'); // base64 -> buffer
      var fileSize = Buffer.byteLength(fileBuffer);
      
      process.on('uncaughtException', function (err) {
        logger(err, error);
      }); 
      
      try {
        socketClient = new net.Socket();
        resBody = "Content-Length: "+fileSize+weol+weol;
        socketClient.connect(spePort, speIp, function() {    // START of OPTIONS
          logger('Get preview size from SPE server');
          var optionText =
            'OPTIONS icap://'+speIp+':'+spePort+'/SYMCScanRespEx-AV ICAP/1.0'+weol+
            'Host: '+speIp+':'+spePort+weol+
            'User-Agent: IT-Kartellet ICAP Client/1.1'+weol+
            'Encapsulated: null-body=0'+weol+weol;
          socketClient.write(optionText);

          // ***********************************************************************
          // Socket Client Responses - OPTIONS
          socketClient.on('connect', function() {
            logger('Socket Connected (OPTIONS): '+speIp+':'+spePort);
          });

          // Options Return
          socketClient.on('data', function(data) {
            logger('Return from SPE (OPTIONS):'+eol+eol+data);
            var regExOptionReturnCode = /ICAP\/[0-9\.]+ (\d+) /.exec(data);
            var previewSize=4;  // Default Value
            //logger('Default preview size: '+previewSize);    // Debug
            socketClient.destroy(); // Kill socketClient after server's response

            if (regExOptionReturnCode != null) {
              if (regExOptionReturnCode[1] == '200') {    // Check 200 - Options OK
                // Get Preview Size
                var regExPreviewSize = /Preview: (\d+)/.exec(data);
                if (regExPreviewSize != null) {
                  previewSize = regExPreviewSize[1];
                  logger('Preview size from OPTIONS: '+previewSize);
                } else {
                  logger('Preview size by default: '+previewSize);
                }
                
                // ***********************************************************************
                // Scan file - Send ICAP message to SPE
                socketClient = new net.Socket();
                socketClient.connect(spePort, speIp, function() {    // START of Scan File
                  logger('Scanning file "'+req.body['filename']+'"');
                  var sendText = 
                    'RESPMOD icap://'+speIp+':'+spePort+'/SYMCScanRespEx-AV ICAP/1.0'+weol+
                    'Host: '+speIp+':'+spePort+weol+
                    'User-Agent: IT-Kartellet ICAP Client/1.1'+weol+
                    'Preview: '+previewSize+weol+
                    'Encapsulated: res-hdr=0, res-body='+resBody.length+weol+weol+
                    resBody+
                    previewSize.toString(16)+weol;
                    
                  socketClient.write(sendText);
                  logger('Sending message:'+eol+eol+sendText);
                  logger('Whole Buffer Size: '+fileSize);
                  
                  if (fileSize > previewSize) {
                    socketClient.write(fileBuffer.slice(0, previewSize), 'binary');  // Send Preview
                    socketClient.write(weol+'0'+weol+weol);
                  } else {
                    socketClient.write(fileBuffer, 'binary');  // Send All
                    socketClient.write(weol+'0; ieof'+weol+weol);
                  }

                  // ***********************************************************************
                  // Socket Client Responses - Scanning
                  socketClient.on('connect', function() {
                    logger('Socket Connected (Sacnning): '+speIp+':'+spePort);
                  });

                  // Scanning Return
                  socketClient.on('data', function(data) {
                    var scanCompleted = false;
                    var regExReturnCode = /ICAP\/[0-9\.]+ (\d+) /.exec(data);

                    if (regExReturnCode == null) {
                      scanCompleted = true;
                    } else {
                      if (regExReturnCode[1] == '100') {
                        var remainingSize = parseInt(fileSize, 10) - parseInt(previewSize, 10);
                        
                        // Send Remaining
                        var currPos = previewSize;
                        var sliceSize = std_send_size;
                        var sliceEndPos = parseInt(previewSize, 10) + parseInt(std_send_size, 10);

                        // Send file slice by slice
                        while (parseInt(currPos, 10) < parseInt(fileSize, 10)) {
                          if (parseInt(currPos, 10) + parseInt(sliceSize, 10) <= parseInt(fileSize, 10)) {    // Slice End Position within File Size
                            sliceSize = parseInt(std_send_size, 10);
                            sliceEndPos = parseInt(currPos, 10) + parseInt(std_send_size, 10);
                          } else {    // Slice End Position larger than File Size
                            sliceSize = parseInt(fileSize, 10) - parseInt(currPos, 10);
                            sliceEndPos = fileSize;
                          }
                          
                          //logger('----currPos:'+currPos+'; sliceSize:'+sliceSize+'; sliceEndPos:'+sliceEndPos+'----');    // Debug
                          socketClient.write(sliceSize.toString(16)+weol);
                          socketClient.write(fileBuffer.slice(currPos, sliceEndPos), 'binary');  // Send Remaining
                          socketClient.write(weol);

                          currPos = parseInt(currPos, 10) + parseInt(sliceSize, 10);
                        }
                        //logger('Slice:'+fileBuffer.slice(previewSize));    // Debug
                        
                        socketClient.write(weol+'0'+weol+weol);
                        logger('End of Send');
                      } else if (regExReturnCode[1] === '200') {
                        scanCompleted = true;
                      } else {
                        scanCompleted = true;
                      }
                    }

                    if (scanCompleted == true) {
                      //logger('Socket Received: ' + data);    // Debug
                      logger('Scanning Result of "'+req.body['filename']+'":'+eol+eol+data);
                      res.send(data); 
                      updateLog(req, data.toString());
                      socketClient.destroy(); // Kill socketClient after server's response
                    }
                  });
                  
                  socketClient.on('error', function(error) {
                    errorMessage = 'Error: Socket Error (Scanning): '+error;
                    logger(errorMessage, error);
                    res.send(errorMessage);    // Output error to end the web service
                    updateLog(req, errorMessage);
                  });

                  socketClient.on('close', function() {
                    logger('Socket Closed (Scanning): '+speIp+':'+spePort+eol+eol);
                  });
                });    // END of Scan File
              } else {    // OPTIONS returns not 200
                errorMessage = 'Error: SPE returns error from OPTIONS. Return code:'+regExOptionReturnCode[1];
                logger(errorMessage, error);
                res.send(errorMessage);    // Output error to end the web service
                updateLog(req, errorMessage);
              }
            } else {    // OPTIONS returns unknown
              errorMessage = 'Error: SPE returns unkown error from OPTIONS.';
              logger(errorMessage, error);
              res.send(errorMessage);    // Output error to end the web service
              updateLog(req, errorMessage);
            }
          });

          socketClient.on('error', function(error) {
            logger('Socket Error (OPTIONS): '+error, error);
            res.send('Error: Socket Error (OPTIONS): '+error);    // Output error to end the web service
            updateLog(req, error);
          });

          socketClient.on('close', function() {
            logger('Socket Closed (OPTIONS): '+speIp+':'+spePort+eol+eol);
          });
        });    // END of OPTIONS
      } catch (e) {
        logger(e.toString(), error);
      }
    }  // END of Check attachment pattern
  }    // END of CheckEmpty
});    // END of POST

app.get('/version', (req, res) => {
  res.send(version.version);
});

function updateLog(req, message) {

  const regExit = /ICAP\/[0-9\.]+ (\d+) /.exec(message.toString());
  const returnCode = regExit == null ? '' : regExit[1]; // 200 or 403, or nothing

  var fileId = "";

  if (req.body['applicationId']==null || req.body['applicationId']=='') {
    fileId = req.body['applicationId'];
  } else {
    fileId = req.body['fileId'];
  }

  var systemId = req.body['systemId'] || "EASE-WEB";

  axios({
    method: 'post',
    url: 'http://api.ease-sit3.intraeabdc/ease-api/spe/log',
    data: {
      fileId: fileId,
      attachment: req.body['attachment'],
      filename: req.body['filename'],
      response: message.toString(),
      systemId: systemId,
      responseCode: returnCode
    }
  }).then(res => {
    // let checkout = res.data;
    orderSessionId = res.data.session.id;
    ordersuccessIndicator = res.data.successIndicator;
    log4jUtil.log('info', orderSessionId);
    log4jUtil.log('info', ordersuccessIndicator);
  }).catch(err => {
    console.log(err.res);
  });
}

function logger(message, logType) {
  if (logType == null) logType='INFO';
  console.log(dateFormat(Date(), 'yyyy-mm-dd HH:MM:ss')+' ['+logType+'] '+message);
}

